# timer-app

### What do you need: 
- [Node.js LTS][id/name]  

[id/name]: https://nodejs.org/en/
- Yarn

- Admin access



### How to run
    git clone https://gitlab.com/tambal1/new-timer-app.git
    cd new-timer-app
    yarn
    yarn start
